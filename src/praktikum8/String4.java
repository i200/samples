public class String4 {
	public static void main(String[] args) {
		String s;
		
		System.out.print("Oled rahul? ");
		s = TextIO.getlnString();
		
		// Keerame stringis ko~ik ta"hed va"iketa"htedeks
		s = s.toLowerCase();
		
		// Loikame va"lja esimesed 2 ma"rki
		s = s.substring(0, 2);
		
		if (s.equals("ja")) {
			System.out.println("Oi kui nunnu!");
		}
		else if (s.equals("ei")) {
			System.out.println("Mahh, pidur oled va?");
		}
		else {
			System.out.println("Tropp, sa oleks pidanud vastama ei voi ja!");
		}
	}
}
