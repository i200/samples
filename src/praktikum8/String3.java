public class String3 {
	public static void main(String[] args) {
		String s;
		
		System.out.print("Sisesta midagi: ");
		s = TextIO.getlnString();
		
		// Stringe EI SAA võrrelda nagu primitiivtüüpe kahe võrdusmärgiga ==
		// Tuleb kasutada equals meetodit
		if (s.equals("tere")) {
			System.out.println("Tere ise kah!");
		}
		else {
			System.out.println("Sa sisestasid: " + s);
		}
	}
}
