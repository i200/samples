/**
 * Näide, kuidas konkreetse sisuga massiivi deklareerida ning
 * sisu ekraanile trükkida.
 */
public class MassiivNaide {
	public static void main(String[] args) {
		String[] nimed = {"Peeter", "Ants", "Mari"};
		int i;
		
		for (i = 0; i < nimed.length; i = i + 1) {
			System.out.println(nimed[i]);
		}
	}
}
