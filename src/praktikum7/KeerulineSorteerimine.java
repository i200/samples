import java.util.*;

/* 
 * Hoiatus!
 * 
 * See na"ide on mo~eldud kodanikele, kes juba oskavad natuke
 * programmeerida ja kellele tavalised massiiviylesanded tunduvad
 * liiga lihtsad. Kui Sa tavaliste ylesannetega ha"sti hakkama ei saa,
 * a"ra parem proovi sellest na"itest aru saada. 
 */
public class KeerulineSorteerimine {
	public static void main(String[] args) {
		// see klass vo~iks vabalt asuda eraldi failis
		class Sportlane {
			public double tulemus;
			public String nimi;
		}

		Sportlane[] m = new Sportlane[10];
		int i;
		
		System.out.println("Kysin 10 tulemust ja nime.");
		System.out.println("Sisesta palun tulemus tyhik nimi.");
		System.out.println("Naiteks: 17.3 Mati Maasikas");
		for (i = 0; i < m.length; i = i + 1) {
			Sportlane tmp = new Sportlane();
			System.out.print("Sportlane nr. " + (i+1) + " : ");
			tmp.tulemus = TextIO.getDouble();
			tmp.nimi = TextIO.getlnString();
			m[i] = tmp;
		}
		
		// Sorteerime tulemuste ja"rgi kahanevas ja"rjekorras
		Arrays.sort(m, new Comparator<Sportlane>() {
			/* compare peab tagastama numbri mis on:
			   0   kui s1 = s2
			   < 0 kui s1 < s2
			   > 0 kui s2 < s1
			 */
			public int compare(Sportlane s1, Sportlane s2) {
				if (s1.tulemus > s2.tulemus)
					return -1;
				else if (s1.tulemus < s2.tulemus)
					return 1;
				else
					return 0;
			}
		});
		
		// Trykime massiivi sisu ekraanile
		for (i = 0; i < m.length; i = i + 1) {
			System.out.println(m[i].tulemus + "\t" + m[i].nimi);
		}
	}
}
