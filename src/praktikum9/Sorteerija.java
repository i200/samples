import java.util.ArrayList;
import java.util.Collections;

public class Sorteerija {
	public static void main(String[] args) {
		ArrayList <String> v = new ArrayList<String>();
		String nimi;
		int i;
		
		do {
			System.out.print("Sisesta nimi (tyhi rida lopetab): ");
			nimi = TextIO.getlnString();
			if(!nimi.equals("")) {
				v.add(nimi);
			}
		} while(!nimi.equals(""));
		
		Collections.sort(v);
		
		for(i = 0; i < v.size(); i = i + 1) {
			System.out.println(v.get(i));
		}
	}
}
