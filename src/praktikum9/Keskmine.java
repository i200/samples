import java.util.ArrayList;

public class Keskmine {
	public static void main(String[] args) {
		ArrayList <Double> v = new ArrayList<Double>();
		double nr;
		double sum = 0;
		int i;
		
		do {
			System.out.print("Sisesta number (0 lopetab): ");
			nr = TextIO.getlnDouble();
			if(nr != 0) { 
				v.add(nr);
			}	
		} while(nr != 0);
		
		for(i = 0; i < v.size(); i = i + 1) {
			sum = sum + v.get(i);
		}
		
		System.out.println("Keskmine oli: " + (sum / v.size()));
	}
}
