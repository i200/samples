public class Kaart {
    // enum tüübid masti ja väärtuse jaoks, võivad olla eraldi failides nagu klassidki
    public enum Mast {ARTU, RISTI, RUUTU, POTI}
    public enum Vaartus {
        YKS("1"), KAKS("2"), ... KUNN("K");
        private String taht;
        private Vaartus(String taht) {
            this.taht = taht;
        }
    }

    // selle konkreetse kaardi mast ja väärtus
    private final Mast mast;
    private final Vaartus vaartus;

    // konstruktor-meetod
    public Kaart(Mast mast, Vaartus vaartus) {
        this.mast = mast;
        this.vaartus = vaartus;
    }
    // konkreetse kaardi väljaprindil tekstiks teisendamine
    public String toString() {
        return String.format("%s %s", mast.name(), vaartus.taht);
    }
}
