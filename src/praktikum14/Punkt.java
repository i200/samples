
public class Punkt {

    public int x;
    public int y;
    
    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public String toString() {
        return String.format("Punkt(%d,%d)", x, y);
    }
    
}
