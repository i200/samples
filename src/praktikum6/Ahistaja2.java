public class Ahistaja2 {
	public static void main(String[] args) {
		int n;
		
		do {
			System.out.print("Sisesta number vahemikus 1 - 10: ");
			n = TextIO.getlnInt();
		} while(n < 1 || n > 10);
		
		System.out.println("Taname meeldiva koostoo eest!");
	}
}
